##### A Simple ChatBot

In this project a chatbot is created using lstm.

### How to Run
- First create a csv file containing sentences ``input.txt``
- Update its path in the program
- Execute the following command

	``python main.py``

- It will output an ``output.txt`` file containing ten sentences
